Halcion is another drug from the Year of the Pills series. Number 5 or 6 I believe, but I'm too sleepy to count now.

I should thank Gary Dumaigne, who actually put me inside the nightmare in his own head in order for me to learn how to appreciate the "grunge movement". Here were Gary's words to me: "There is no grunge movement, but pretend there is. Design it first, and come up with your reasoning later. It sort of works like a curriculum vitae. First you do the work, and then you tell people why it's a reason to appreciate you. With that A storming out like that, you sure as hell can bullshit your way into any mind, pants, or hip circles.

I pretended that I loved it, even if and when it gave me goosebumps. This way I tried to belong. All the kids want to be Carson. It's useless to try understanding their why-the-hell; I just know that they do. So I left needless points in there. I left bad curves unfixed. I did use autohint. I did slant it 15 degrees. I did use wine to wash down the Tylenol. If there were a movement, those kids would have a cheesy slogan like: Very cool to trash the rule.

Once I had a motto. Here's what it was: So it goes.

I belonged enough for now. Maybe I'll complete the character sets when I'm peaceful enough to want this sort of speed again. I should go back to my geometric stuff with Jeri.

Focus pocus. It's all fun and games.

'
 