using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoPersonaje : MonoBehaviour
{
    public float velocidad = 10;

    public float limiteSuperior;
    public float limiteDerecho;

    void Update()
    {
        Desplazamiento();
    }

    void Desplazamiento()
    {
        // Movimiento
        float x = Input.GetAxis("Horizontal") * velocidad;
        float z = Input.GetAxis("Vertical") * velocidad;

        float desplazamientoX = x * Time.deltaTime;
        float desplazamientoZ = z * Time.deltaTime;

        bool comprobacionSuperior  = (transform.position.z + desplazamientoZ) <  limiteSuperior;
        bool comprobacionInferior  = (transform.position.z + desplazamientoZ) > -limiteSuperior;
        bool comprobacionDerecha   = (transform.position.x + desplazamientoX) <  limiteDerecho ;
        bool comprobacionIzquierda = (transform.position.x + desplazamientoX) > -limiteDerecho ;

        if (comprobacionSuperior  && comprobacionInferior && comprobacionDerecha && comprobacionIzquierda)
        {
            transform.Translate(desplazamientoX, 0, desplazamientoZ);
        }

        // Rotaci�n
        if(x!=0 || z != 0) { // Que solo rote cuando X y Z no son 0
            Vector3 direccion =  new Vector3(x, 0, z);

            //transform.GetChild(0).rotation = Quaternion.LookRotation(direccion);

            Transform ruedas = transform.GetChild(0);

            Quaternion rotacionFinal = Quaternion.LookRotation(direccion);
            ruedas.transform.rotation = Quaternion.Slerp(ruedas.transform.rotation, rotacionFinal, Time.deltaTime);

        }
    }

}
