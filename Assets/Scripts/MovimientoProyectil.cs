using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoProyectil : MonoBehaviour
{

    public float velocidad = 10;

    private void Start()
    {
        Destroy(gameObject, 3);
    }

    void Update()
    {
        transform.Translate(Vector3.up * Time.deltaTime * velocidad);
    }
}
