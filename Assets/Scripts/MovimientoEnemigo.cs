using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoEnemigo : MonoBehaviour
{
    Transform jugador;
    public float velocidad;
    private void Start()
    {
        jugador = GameObject.Find("Jugador").transform;
    }

    void Update()
    {

        transform.LookAt(jugador);
        transform.Translate(Vector3.forward* velocidad * Time.deltaTime);
    }
}
