using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Text crono;

    private void Update()
    {
        crono.text = Time.timeSinceLevelLoad.ToString("f2");
    }

    public void GameOver()
    {
        PlayerPrefs.SetFloat("tiempoPartida", Time.timeSinceLevelLoad);
        SceneManager.LoadScene(2);
    }
}
