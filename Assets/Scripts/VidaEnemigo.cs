using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidaEnemigo : MonoBehaviour
{
    public GameObject particulasExplosion;

    private void OnCollisionEnter(Collision collision)
    {
        Instantiate(particulasExplosion, transform.position, particulasExplosion.transform.rotation);

        FindObjectOfType<AudioManager>().ReproducirExplosion();

        Destroy(collision.gameObject);
        Destroy(gameObject);
    }
}
