using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource musica;
    public AudioSource fxDisparo;
    public AudioSource fxExplosion;

    public void ReproducirDisparo()
    {
        fxDisparo.Play();
    }

    public void ReproducirExplosion()
    {
        fxExplosion.Play();
    }
}
